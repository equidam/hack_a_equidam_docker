#============== OWL ================#


LOCAL DEPENDENCIES INSTALL
================================
1. Install ansible `pip install ansible` - if you get errors about url requesets then try pip installing one of these `urllib3`, `pyopenssl`, `ndg-httpsclient`, and `pyasn1`
2. Install aws `pip install awscli`
3. Add dockerpuller profile to aws with `aws configure --profile dockerpuller`, with details from ./vars/api_keys.yml

LOCAL VM PROVISION
===============================
Will set up the vagrant with infrastructure containers (flu cas pgres) running already

1. You may (usually) need to clear the ~/.ssh/knowhosts of the 127.0.0.1 entries
2. Run `ansible-playbook ./localsetup.yml` which up's the vagrant
3. Run `ansible-playbook --private-key=~/.vagrant.d/insecure_private_key -i ./inventory/hosts.vm playbook.yml`

Run playbook with extra options
------------------------------
* Run only parts of the playbook with `--tags="tag1,tag2"`
* Add verbose logging with `-vvv`

DATABACK UP/INJECT
=====================
Injection
-----------------
* see `cas_inject.yml` & its initial comments.
Backup
-----------------
* see `cas_backup.yml` & its initial comments.

Doing Actual Dev
===================================
* Use `./boot_test_???.sh` to boot into a crawler/munger with volume mounts onto the crawlers/munger files for easy testing
* The infrastructure containers should always be running so shouldn't need to use init scripts - just use the playbook.yml

Pushing New Images to AWS
===================================
1. Make a repo on AWS EC2 (https://equidam-aws.signin.aws.amazon.com/console) with name `equidam/img_name` (don't follow the commands there though...)
2. Not in vagrant. With a user (in terminal) that you have set the access key, secret key, and default region. 
        Do `aws ecr get-login --region eu-west-1`; copy the outputted command.
3. In your environment where you use docker (i.e. in VM as vagrant), do the copied command.
4. Do `docker build -t 656576915734.dkr.ecr.eu-west-1.amazonaws.com/equidam/imagename .`
5. Do `docker push 656576915734.dkr.ecr.eu-west-1.amazonaws.com/equidam/somename:latest`
6. Re provision the server using the playbook, using 'ansible-playbook --private-key=~/.ssh/dbc-key-pair-ireland.pem -i ./inventory/hosts playbook.yml --tags="dockerimages"'
 and other inits if changed infrastucture image (flu, cas, pgres) that are already running.

AWS Server SETUP
===================================
1. Set servers public IP address in `./host_vars/owl.equidam.com` if new server
2. Get pem key from `dbc@equidam.com` or `fc@equidam.com`
* TO ssh into AWS server: `ssh -i ~/.ssh/dbc-key-pair-ireland.pem ubuntu@ec2-52-209-125-180.eu-west-1.compute.amazonaws.com`
* TO provision AWS server: `./getlogin.sh`
    * `ansible-playbook --private-key=~/.ssh/dbc-key-pair-ireland.pem -i ./inventory/hosts playbook.yml -vvv`

------------------------------
* Run only parts of the playbook with `--tags="tag1,tag2"`
* Add verbose logging with `-vvv`

Bonus: (Vagrant port problem solve)
--------------------------
if there is a problem it can be that the port of the vagrant ssh is not corrent if this is the case:
    vagrant destroy
then remove the  config.ssh.port line from the host vars files 
then vagrant ssh-config
then update the file owl.equidam.dev so that ansible_ssh_port: matchs the pport on the vagrant ssh-config