---
# ssh -i "dbc-key-pair-ireland.pem" ubuntu@ec2-54-171-142-69.eu-west-1.compute.amazonaws.com
# ansible-playbook --private-key=~/.ssh/dbc-key-pair-ireland.pem -i ./inventory/hosts playbook.yml


# For designating dev only tasks with 'when: in_development'
in_development: False
in_production: True 

# For host connection to AWS server
ansible_ssh_host: 52.209.125.180
ansible_ssh_user: ubuntu

# For dev environment variables
docker_user: ubuntu
