---
# TO provision VM: 'ansible-playbook --private-key=~/.vagrant.d/insecure_private_key -i ./inventory/hosts.vm playbook.yml -vvv'
# TO ssh into AWS server: 'ssh -i ~/.ssh/dbc-key-pair-ireland.pem ubuntu@ec2-54-194-189-52.eu-west-1.compute.amazonaws.com'
# TO provision AWS server: `./getlogin.sh` # if going to pull docker images 
# 'ansible-playbook --private-key=~/.ssh/dbc-key-pair-ireland.pem -i ./inventory/hosts playbook.yml -vvv --tags=" "'
- hosts: all
  remote_user: root
  become: yes
  vars_files:
    - vars/api_keys.yml
    - vars/docker_login.yml
    - vars/users.yml
    - vars/images.yml
    - vars/databases.yml
    - vars/crawl_cron.yml
    - vars/munge_cron.yml
  vars:
    docker_group_members: ["{{ docker_user }}"]
    pip_install_docker_py: true
    pip_version_docker_py: "1.9.0"
  pre_tasks:
    - name: Update packages
      apt: update_cache=yes
    - name: Upgrade packages
      apt: upgrade=dist


  roles:
    - role: angstwad.docker_ubuntu
      tags:
        - dockerinit
    - role: ansible-swapfile
      tags:
        - systeminit

  # Start task
  tasks: 
    # Initialise sequence
    - name: Create equidam group with CONVENTION GID 2001
      group: name={{ equidam }} gid=2001 state=present
      tags:
        - systeminit
    - name: Create the equidam user with CONVENTION UID 2001
      user: name={{ equidam }} uid=2001 state=present
      tags:
        - systeminit
    - name: Copy required and useful scripts into system
      copy: src=equidam/{{ item }} dest={{ homedir }} owner={{ equidam }} group={{ equidam }} mode=0755
      with_items:
        - "{{ pgres_db_inits }}"
      tags:
        - systeminit
    - name: Create volume directories for persitent docker mount binds
      file: path={{ homedir }}/volumes/{{ item }} state=directory owner={{ equidam }} group={{ equidam }} recurse=yes
      with_items:
        - log
        - pgres_data
      tags: 
        - systeminit

    # Docker environment initialise & debugs
    - name: Re/start docker
      service: name=docker state=restarted
      tags:
        - dockerinit
    - shell: "docker info"
      register: docker_info
      tags:
        - dockerinit
    - debug: var=docker_info verbosity=2
      tags:
        - dockerinit

      # docker_login_command is only valid for 24 hours
      # use getlogin.sh to refresh it
        # if 'invalid token' when run getlogin.sh then need to update `aws configure --profile dockerpuller` 
    - name: Login into AWS ecr with docker_puller
      shell: "{{ docker_login_command }}"
      register: login_output
      tags: 
        - dockerimages
    - debug: var=login_output verbosity=2
      tags: 
        - dockerimages
      # Pull docker images
    - name: 'Pull fluentd image'
      docker_image:
        name: "{{ flu_img }}"
        force: yes
        tag: latest 
      tags:
        - dockerimages
    - name: 'Pull postgres image'
      docker_image:
        name: "{{ pgres_img }}"
        force: yes
        tag: latest 
      tags:
        - dockerimages
    - name: 'Pull php image'
      docker_image: 
        name: "{{ php_img }}"
        force: yes
        tag: latest 
      tags:
        - dockerimages
    # Infrastructure initialisation
      # could add restart policies to these
      # start fluentd
    - name: 'Start fluentd'
      docker:
        name: flu
        image: "{{ flu_img }}"
        ports: 24224:24224
        state: started
        detach: true
        volumes:
          - "{{ homedir }}/volumes/log:/fluentd/log"
      tags:
          - infrastructureinit
      # start postgres
    - name: 'Start postgres'
      docker:
        name: pgres
        image: "{{ pgres_img }}"
        state: started
        detach: true
        log_driver: fluentd
        volumes:
          - "{{ homedir }}/volumes/pgres_data:/var/lib/postgresql/data"
      tags:
          - pgresinit
        # Database initialise
    - name: Check until pgres has finished boot up sequence
      action: shell docker exec pgres psql -U postgres -d postgres -c "SHOW SERVER_VERSION;"
      register: result
      until: "'psql: could not connect to server' not in result.stderr"
      retries: 20
      delay: 5
      tags:
          - pgresinit
    - name: Feed PQSL setup file into pgres
      shell: |
              echo "Piping {{ homedir }}/{{ pgres_db_inits }}/hack_setup.sql"
              docker exec -i pgres psql -U postgres < {{ homedir }}/{{ pgres_db_inits }}/hack_setup.sql
      tags: 
        - pgresinit
    - name: Feed PQSL tables SQL files into pgres as database user equidam
      shell: |
            for file_path in $(ls {{ homedir }}/{{ pgres_db_inits }}/0* | sort -n)
            do
                echo "Piping $file_path into psql"
                docker exec -i pgres psql -U equidam -d hackequidam < $file_path
            done
      tags: 
        - pgresinit
      # start api
    - name: 'Start php'
      docker:
        name: phpfpm
        image: "{{ php_img }}"
        state: restarted
        detach: true
        links:
          - "pgres:pgres" 
        ports:
          - "9000"
        log_driver: fluentd
        volumes:
          - "/vagrant/hack_equidam/htdocs/:/var/www/html/"
          - "/vagrant/hack_equidam/composer.phar:/home/composer.phar"
      tags:
          - phpinit

    # NGINx
    - name: 'Make directories for nginx & ssl'
      file: 
        path: /etc/{{ item }}
        state: directory
        mode: 0600
      with_items:
        - nginx/conf.d
        - ssl/equidam
      tags:
        - nginx
    - copy: 
        src: ./equidam/nginx_conf/{{ item }}
        dest: /etc/nginx/conf.d/{{ item }}
        owner: root
        force: yes
        mode: 600
      with_items:
        - hack.equidam.com.conf
      tags:
          - nginx
    - copy:
        src: "{{ item }}"
        dest: /etc/ssl/equidam
        owner: root
        mode: 600
      with_fileglob:
        - ./equidam/nginx_conf/ssl/*
      tags:
          - nginx_conf
    - name: 'run nginx'
      docker:
        name: equ-nginx
        image: nginx:stable
        state: restarted
        detach: true
        links: 
          - phpfpm
        volumes:
          - /etc/nginx/conf.d/:/etc/nginx/conf.d/
          - /etc/ssl/equidam/:/etc/ssl/equidam/
        log_driver: fluentd
        ports:
          - "80:80"
          - "443:443"
      tags:
          - nginx

