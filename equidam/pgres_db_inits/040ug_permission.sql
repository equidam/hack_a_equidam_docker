CREATE TABLE ug_permission (
    perm_id serial PRIMARY KEY,   
    ugid int REFERENCES usergroup(ugid),
    rid int REFERENCES user_defined_r(rid),
    added Date DEFAULT CURRENT_TIMESTAMP,
    database text NOT NULL,
    read boolean NOT NULL DEFAULT false,
    write boolean NOT NULL DEFAULT false,
    discover boolean NOT NULL DEFAULT false
); 
