CREATE TABLE usergroup (
    ugid serial PRIMARY KEY,   
    uid int REFERENCES login_user(uid),
    added Date DEFAULT CURRENT_TIMESTAMP
); 
insert into usergroup(uid) values (2);
