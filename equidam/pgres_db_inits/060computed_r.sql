CREATE TABLE computed_r (
    crid serial PRIMARY KEY,   
    added Date DEFAULT CURRENT_TIMESTAMP,
    ugid int REFERENCES usergroup(ugid),
    definition text,
    error text,
    result text
); 
