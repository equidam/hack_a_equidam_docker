CREATE TABLE default_r (
    defid serial PRIMARY KEY,   
    added Date DEFAULT CURRENT_TIMESTAMP,
    context text,
    resources text
); 
