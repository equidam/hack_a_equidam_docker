CREATE TABLE profile (
    prof_id serial PRIMARY KEY,   
    ugid int REFERENCES usergroup(ugid),
    name text,
    company text
); 
