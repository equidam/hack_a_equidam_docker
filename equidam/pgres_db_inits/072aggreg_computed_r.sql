CREATE TABLE aggreg_computed_r (
    acid serial PRIMARY KEY,   
    added Date DEFAULT CURRENT_TIMESTAMP,
    resources text
); 
