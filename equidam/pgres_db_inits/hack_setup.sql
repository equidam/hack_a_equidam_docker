-- This is pipe directly into psql in the pgres container: it should do all initialising of the database 
-- The other numbered files in this directory are piped into psql as user equidam in numbered order (respect the numbering and it will just work)
-- See the pg_hba.conf: this is where the permission for connecting to the db are configured
-- See the postgres docker-entrypoint.sh for more on the initialize
    -- the pg_hba.conf that post actually runs is made in the entrypoint script, the one in this dict is just what it should be
    -- Loads of bullshit happens in that entry point and it could do a custom and simpler one being written (one day...).
CREATE USER equidam WITH PASSWORD 'pzRHqudHo5AZsIbMCFWTFuBqzz6cJ5pyyEka2KD2gj40';
CREATE DATABASE hackequidam with owner equidam;
GRANT ALL ON DATABASE hackequidam TO equidam;
GRANT ALL ON DATABASE hackequidam TO postgres;
REVOKE CONNECT ON DATABASE hackequidam FROM PUBLIC;
REVOKE ALL ON DATABASE hackequidam FROM PUBLIC; 
