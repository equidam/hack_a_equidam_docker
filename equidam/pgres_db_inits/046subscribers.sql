CREATE TABLE subscriber (
    sid serial PRIMARY KEY,   
    ugid int REFERENCES usergroup(ugid),
    startdate Date DEFAULT CURRENT_TIMESTAMP,
    enddate Date DEFAULT CURRENT_TIMESTAMP
); 
