DROP TABlE company_index;
DROP TABlE financial;
DROP TABlE company;
CREATE TABLE company(
    cid serial PRIMARY KEY ,   
    ugid int REFERENCES usergroup(ugid),
    added Date DEFAULT CURRENT_TIMESTAMP
); 
ALTER TABLE public.company OWNER TO equidam;
CREATE TABLE company_index (
    cindex_id serial PRIMARY KEY ,   
    cid int REFERENCES company(cid),
    ugid int REFERENCES usergroup(ugid),
    current_state text,
    added Date DEFAULT CURRENT_TIMESTAMP
); 
ALTER TABLE public.company_index OWNER TO equidam;
