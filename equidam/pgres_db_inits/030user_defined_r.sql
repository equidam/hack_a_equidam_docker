CREATE TABLE user_defined_r (
    rid serial PRIMARY KEY,   
    ugid int REFERENCES usergroup(ugid),
    added Date DEFAULT CURRENT_TIMESTAMP,
    resources text
); 
